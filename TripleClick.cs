using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Tao.OpenGl;
using Tao.Sdl;

public delegate void FollowerFunction(string keyName);

public class TripleClick
{
	public Dictionary<string,KeyHistory> keyHistoryDictionary = new Dictionary<string,KeyHistory>();

	public static int Main()
	{
		Console.WriteLine("Hi!");
		Console.WriteLine("SystemInformation.DoubleClickTime is {0}.\n", SystemInformation.DoubleClickTime);

		TripleClick tripleClick = new TripleClick();

		if (Sdl.SDL_Init(Sdl.SDL_INIT_VIDEO) < 0)
		{
			Console.WriteLine("SDL could not be initialized: {0}\n", Sdl.SDL_GetError());
			return 1;
		}
		Console.WriteLine("SDL initialized.");

		IntPtr display = (Sdl.SDL_SetVideoMode(800,600,16,Sdl.SDL_SWSURFACE));
		if (display == IntPtr.Zero)
		{
			Console.WriteLine("Could not open a window 800x600px: {0]\n", Sdl.SDL_GetError());
			return 2;
		}
		Console.WriteLine("Base-Surface created.");

//	Triple-Click-Action:

		int quit = 0;
		Sdl.SDL_Event myEvent;

		while(quit == 0)
		{
			while (Sdl.SDL_PollEvent(out myEvent) != 0)
			{
				switch(myEvent.type)
				{
					case Sdl.SDL_KEYDOWN:
						Console.WriteLine("Press: ");
						Console.WriteLine(" Name: {0}\n", Sdl.SDL_GetKeyName(myEvent.key.keysym.sym));
						break;
					case Sdl.SDL_KEYUP:
						Console.WriteLine("Release: ");
						Console.WriteLine(" Name: {0}\n", Sdl.SDL_GetKeyName(myEvent.key.keysym.sym));
						tripleClick.genericKeyRelease(Sdl.SDL_GetKeyName(myEvent.key.keysym.sym));
						break;
					case Sdl.SDL_MOUSEBUTTONDOWN:
						Console.WriteLine("Press: ");
						switch(myEvent.button.button)
						{
							case Sdl.SDL_BUTTON_LEFT:
								Console.WriteLine(" Name: Left Mouse Button\n");
								break;
							case Sdl.SDL_BUTTON_RIGHT:
								Console.WriteLine(" Name: Right Mouse Button\n");
								break;
							case Sdl.SDL_BUTTON_MIDDLE:
								Console.WriteLine(" Name: Middle Mouse Button\n");
								break;
							case Sdl.SDL_BUTTON_WHEELUP:
								Console.WriteLine(" Name: Mouse Wheel Up Button\n");
								break;
							case Sdl.SDL_BUTTON_WHEELDOWN:
								Console.WriteLine(" Name: Mouse Wheel Down Button\n");
								break;

						}
						break;
					case Sdl.SDL_MOUSEBUTTONUP:
						Console.WriteLine("Release: ");
						switch(myEvent.button.button)
						{
							case Sdl.SDL_BUTTON_LEFT:
								Console.WriteLine(" Name: Left Mouse Button\n");
								tripleClick.genericKeyRelease("Left Mouse Button");
								break;
							case Sdl.SDL_BUTTON_RIGHT:
								Console.WriteLine(" Name: Right Mouse Button\n");
								tripleClick.genericKeyRelease("Right Mouse Button");
								break;
							case Sdl.SDL_BUTTON_MIDDLE:
								Console.WriteLine(" Name: Middle Mouse Button\n");
								tripleClick.genericKeyRelease("Middle Mouse Button");
								break;
							case Sdl.SDL_BUTTON_WHEELUP:
								Console.WriteLine(" Name: Mouse Wheel Up Button\n");
								tripleClick.genericKeyRelease("Mouse Wheel Up Button");
								break;
							case Sdl.SDL_BUTTON_WHEELDOWN:
								Console.WriteLine(" Name: Mouse Wheel Down Button\n");
								tripleClick.genericKeyRelease("Mouse Wheel Down Button");
								break;
						}
						break;
					case Sdl.SDL_QUIT:
						quit = 1;
						break;
					default:
						break;	
				}
			}
		}

		return 0;
	}

	public void genericKeyRelease(string keyName)
	{
		if (keyHistoryDictionary.ContainsKey(keyName) == false)
		{
			KeyHistory tempKeyHistory = new KeyHistory();

			tempKeyHistory.firstRelease  = DateTime.Now.Subtract(TimeSpan.FromSeconds(1));
			tempKeyHistory.secondRelease = DateTime.Now.Subtract(TimeSpan.FromSeconds(1));
			tempKeyHistory.thirdRelease  = DateTime.Now.Subtract(TimeSpan.FromSeconds(1));
			tempKeyHistory.firstFollowerFunction  = new FollowerFunction(genericFollowerFunction);
			tempKeyHistory.secondFollowerFunction = new FollowerFunction(genericFollowerFunction2);
			tempKeyHistory.thirdFollowerFunction  = new FollowerFunction(genericFollowerFunction3);

			keyHistoryDictionary.Add(keyName,tempKeyHistory);
		}

		int fastHitCounter = 1;
		TimeSpan durationAfterfirstRelease;
		TimeSpan durationAftersecondRelease;

		keyHistoryDictionary[keyName].firstRelease  = keyHistoryDictionary[keyName].secondRelease;
		keyHistoryDictionary[keyName].secondRelease = keyHistoryDictionary[keyName].thirdRelease;
		keyHistoryDictionary[keyName].thirdRelease  = DateTime.Now;

		durationAftersecondRelease = keyHistoryDictionary[keyName].thirdRelease  - keyHistoryDictionary[keyName].secondRelease;
		durationAfterfirstRelease  = keyHistoryDictionary[keyName].secondRelease - keyHistoryDictionary[keyName].firstRelease;

		if (durationAftersecondRelease.TotalMilliseconds < SystemInformation.DoubleClickTime)
		{
			fastHitCounter = 2;

			if (durationAfterfirstRelease.TotalMilliseconds < SystemInformation.DoubleClickTime)
			{
				fastHitCounter = 3;
			}
		}

		if (fastHitCounter == 1) keyHistoryDictionary[keyName].firstFollowerFunction(keyName);
		if (fastHitCounter == 2) keyHistoryDictionary[keyName].secondFollowerFunction(keyName);
		if (fastHitCounter == 3) keyHistoryDictionary[keyName].thirdFollowerFunction(keyName);
	}

	public void genericFollowerFunction(string keyName)
	{
		Console.WriteLine("This general function was forced by this key: \"{0}\".",keyName);
	}

	public void genericFollowerFunction2(string keyName)
	{
		Console.WriteLine("This general function (DoubleHit!) was forced by this key: \"{0}\".",keyName);
	}

	public void genericFollowerFunction3(string keyName)
	{
		Console.WriteLine("This general function (TripleHit!) was forced by this key: \"{0}\".",keyName);
	}
}

public class KeyHistory
{
	public DateTime firstRelease;
	public DateTime secondRelease;
	public DateTime thirdRelease;
	public FollowerFunction firstFollowerFunction;
	public FollowerFunction secondFollowerFunction;
	public FollowerFunction thirdFollowerFunction;
}
