Project Goals
-------------

- Creating a prototype of a cross-platform detector for multi-taps (e.g. double- / triple-clicks and quick multiple key-strokes)
  using .NET / Mono, SDL, and Tao so that it can be used in the open-source game OpenRA



Results
-------

The created detector works reliably and has now been adopted into the OpenRA project.
For details, see the following commit by Curtis Shmyr:

[https://github.com/OpenRA/OpenRA/commit/9fdfca6ee5669c6550661f9713153288a7f9de27](https://github.com/OpenRA/OpenRA/commit/9fdfca6ee5669c6550661f9713153288a7f9de27
)
